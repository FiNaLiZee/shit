const path = require("path")
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const CopyWebpackPlugin = require("copy-webpack-plugin")
const {VueLoaderPlugin} = require("vue-loader")

const PATHS = {
    src: path.join(__dirname, "/src/main/assets"),
    public: path.join(__dirname, "/assets")
}

module.exports = {
    externals: {
        paths: PATHS
    },
    entry: {
        app: PATHS.src
    },
    output: {
        path: PATHS.public
    },
    devtool: "eval-cheap-module-source-map",
    devServer: {
        overlay: {
            warnings: true,
            errors: true
        }
    },
    module: {
        rules: [{
            test: /\.js$/,
            loader: "babel-loader",
            exclude: "/node_modules/"
        }, {
            test: /\.vue$/,
            loader: "vue-loader",
            options: {
                loader: {
                    scss: "vue-style-loader!css-loader!sass-loader"
                }
            }
        }, {
            test: /\.scss$/,
            use: [
                "style-loader",
                MiniCssExtractPlugin.loader, {
                    loader: "css-loader",
                    options: {sourceMap: true}
                }, {
                    loader: "postcss-loader",
                    options: {sourceMap: true, config: {path: "./postcss.config.js"}}
                }, {
                    loader: "sass-loader",
                    options: {sourceMap: true}
                }
            ]
        }, {
            test: /\.css$/,
            use: [
                "style-loader",
                MiniCssExtractPlugin.loader, {
                    loader: "css-loader",
                    options: {sourceMap: true}
                }, {
                    loader: "postcss-loader",
                    options: {sourceMap: true, config: {path: "./postcss.config.js"}}
                }
            ]
        }]
    },
    resolve: {
        alias: {
            "~": PATHS.src,
            "vue$": "vue/dist/vue.js"
        }
    },
    plugins: [
        new VueLoaderPlugin(),
        new MiniCssExtractPlugin(),
        new CopyWebpackPlugin([{from: `${PATHS.src}/img`, to: "img"}])
    ]
}