<#macro base>
    <!doctype html>
    <html lang="ru">
    <#include "partials/head.ftl">
    <body>
    <#include "partials/preload.ftl">
    <div id="app" v-cloak>
        <#include "partials/header.ftl">
        <section style="min-height: calc(100vh - 89px - 332px)"><#nested/></section>
        <#include "partials/footer.ftl">
    </div>
    </body>
    </html>
</#macro>