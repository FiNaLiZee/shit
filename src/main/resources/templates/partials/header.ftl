<@consts.consts></@consts.consts>

<header>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light">
            <h1><a class="navbar-brand" href="/"><img src="/assets/img/logo.png" alt="logo"></a></h1>
            <!--Media btn-->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
                <span class="navbar-toggler-icon"></span>
            </button>
            <!--Header List-->
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!--Items-->
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="/catalog">{{consts.HEADER[0]}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/articles">{{consts.HEADER[1]}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/start">{{consts.HEADER[2]}}</a>
                    </li>
                    <!--Drop down menu-->
                    <v-drop-down head="Помощь">
                        <v-drop-down-item link="/support/faq">{{consts.HEADER[3]}}</v-drop-down-item>
<#--                        <v-drop-down-item link="/support/about">{{consts.HEADER[4]}}</v-drop-down-item>-->
                        <v-drop-down-item link="/support/contacts">{{consts.HEADER[5]}}</v-drop-down-item>
                    </v-drop-down>
                </ul>
                <!--Search Block-->
                <form action="/search" method="POST" class="form-inline input-group" style="width: 170px;">
                    <input type="search" name="keyword" :placeholder="consts.PLACEHOLDERS[0]" required
                           class="form-control border-dark" style="height: 30px;">
                    <div class="input-group-append">
                        <button class="btn btn-outline-dark fa fa-search" style="height: 30px;" type="submit"></button>
                    </div>
                </form>
                <!--Modal/User block-->
<#--                <v-modal-form></v-modal-form>-->
            </div>
        </nav>
    </div>
</header>