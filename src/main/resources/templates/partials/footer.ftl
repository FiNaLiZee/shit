<@consts.consts></@consts.consts>

<footer class="page-footer stylish-color-dark pt-4">
    <!-- Footer Links -->
    <div class="container text-center text-md-left">
        <div class="row justify-content-between">
            <div class="col-5">
                <h5 class="font-weight-bold text-uppercase mt-1 mb-4">{{consts.FOOTER[0]}}</h5>
                <p>{{consts.FOOTER[1]}}</p>
            </div>
            <div class="row col-4">
                <div class="col-6 ">
                    <h5 class="font-weight-bold text-uppercase mt-1 mb-4">{{consts.FOOTER[2]}}</h5>
                    <ul class="list-unstyled">
                        <li>
                            <a href="/catalog">{{consts.HEADER[0]}}</a>
                        </li>
                        <li>
                            <a href="/articles">{{consts.HEADER[1]}}</a>
                        </li>
                        <li>
                            <a href="/start">{{consts.HEADER[2]}}</a>
                        </li>
                    </ul>
                </div>
                <div class="col-6">
                    <h5 class="font-weight-bold text-uppercase mt-1 mb-3">{{consts.FOOTER[3]}}</h5>
                    <ul class="list-unstyled">
                        <li>
                            <a href="/support/faq">{{consts.HEADER[3]}}</a>
                        </li>
                        <li>
<#--                            <a href="/support/about">{{consts.HEADER[4]}}</a>-->
                        </li>
                        <li>
                            <a href="/support/contacts">{{consts.HEADER[5]}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <!--Share-->
    <div class="ya-share2 text-center mb-3" data-curtain data-shape="round" data-lang="en" data-color-scheme="normal"
         data-services="vkontakte,facebook,odnoklassniki,telegram,twitter,reddit"></div>
    <!--Copyright-->
    <div class="text-center py-3 bg-custom">
        <span>© {{new Date().getFullYear()}} <a href="/">4Greens.com</a></span>
    </div>
</footer>