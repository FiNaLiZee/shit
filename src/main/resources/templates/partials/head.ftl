<head>
    <meta name="charset" content="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="stylesheet" href="/assets/libs/bootstrap/css/bootstrap.min.css">
    <link href="/assets/app.css" rel="stylesheet">
    <script type="text/javascript" src="/assets/libs/vue.js" async></script>
    <script type="text/javascript" src="/assets/app.js" async></script>
    <script type="text/javascript" src="/assets/libs/jquery/jquery.slim.min.js"></script>
    <script type="text/javascript" src="/assets/libs/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/assets/libs/fontawesome.js" async></script>
    <script src="https://yastatic.net/share2/share.js"></script>
    <title>4Greens</title>
</head>