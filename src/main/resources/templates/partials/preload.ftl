<div id="loading-bar">
    <div id="loading-bar-spinner" class="spinner">
        <div class="spinner-icon"></div>
    </div>
</div>

<script>
    window.onload = () => {
        document.getElementById('loading-bar').style.opacity = '0';
        document.getElementById('loading-bar-spinner').style.opacity = '0';
        setTimeout(() => document.getElementById('loading-bar').style.display = 'none', 1200)}
</script>