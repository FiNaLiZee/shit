<#import "../templates.ftl" as templates>
<#import "../constants.ftl" as consts>

<@templates.base>
    <@consts.consts></@consts.consts>
    <script type="application/json">{"articles": ${articles}}</script>
    <script type="application/json">{"catalog": ${catalog}}</script>
    <script type="application/json">{"parentsAliases": ${parentsAliases}}</script>

    <div class="container">
        <main class="row">
            <aside class="col-3 border-right">
                <div class="p-3">
                    <h3 class="d-inline">Фильтры</h3> <i class="fas fa-filter"></i>
                    <form class="mt-3" style="font-size: 17.4px">
                        <div class="mt-2" v-for="(val, key) in consts.FIELDS">
                            <div class="h5">{{val[val.length-1]}}<v-tooltip v-if="key == 'TIME'">{{consts.FIELDS.TIME[2]}}</v-tooltip></div>
                            <div class="form-check" v-for="(alias, index) in val" v-if="index != val.length-1 && key != 'PUBLICATION' && key != 'TIME'">
                                <input type="checkbox" class="form-check-input" :id="key + index"
                                       :value="index" v-model="filters[key]">
                                <label class="form-check-label" :for="key + index">{{alias}}</label>
                            </div>
                            <div class="form-group row mx-0 pb-3 border-bottom" v-if="key == 'PUBLICATION' || key == 'TIME'">
                                <input type="number" class="form-control col-5" :placeholder="val[0]" v-model="filters[key][0]">
                                <span class="col-2" style="padding: 0 10px 0;">—</span>
                                <input type="number" class="form-control col-5" :placeholder="val[1]" v-model="filters[key][1]">
                            </div>
                        </div>
                    </form>
                </div>
            </aside>
            <article class="col-9 p-4">
                <div class="row">
                    <div class="col-4 p-3" v-for="item in filter" :key="item.id">
                        <v-articles-card :alias="item.alias" :title="item.title" :author="item.author"
                                         :format="item.format"
                                         :parents-alias="parentsAliases[item.parent]"/>
                    </div>
                </div>
            </article>
        </main>
    </div>
</@templates.base>