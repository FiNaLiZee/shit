<#import "../templates.ftl" as templates>
<#import "../constants.ftl" as consts>

<@templates.base>
    <@consts.consts></@consts.consts>
    <script type="application/json">{"same": ${same}}</script>
    <script type="application/json">{"current": ${current}}</script>
    <script type="application/json">{"catalog": ${catalog}}</script>
    <script type="application/json">{"articles": ${articles}}</script>
    <script type="application/json">{"parentsAliases": ${parentsAliases}}</script>
    <div class="container">
        <main class="row justify-content-center">
            <!--SideBar-->
            <aside class="col-3 border-right">
                <!--Site map-->
                <div class="p-3">
                    <h3 class="d-inline">{{consts.PAGES.ARTICLE[0]}}</h3> <i class="fas fa-project-diagram"></i>
                    <ul class="mt-3">
                        <v-tree-element v-for="el in tree" :key="el.id" :sub="true" :node="el"></v-tree-element>
                    </ul>
                </div>
            </aside>
            <!--Main-->
            <article class="col-9 p-4">
                <!--Current Title-->
                <div class="d-flex justify-content-between">
                    <a :href="prev" class="btn btn-outline-dark h4" style="width: 38px;"><i class="fas fa-angle-left"></i></a>
                    <h3>{{ cut }}</h3>
                    <a :href="next" class="btn btn-outline-dark h4" style="width: 38px;"><i class="fas fa-angle-right"></i></a>
                </div>
                <!--Item Preview-->
                <section class="row ml-0 pt-4">
                    <!--Poster-->
                    <div class="col-5">
                        <img :src="'/assets/img/catalog/' + parentsAliases[current.parent] + '/' + consts.FORMAT_CATEGORY[current.format] + '/' + current.alias + '.'+type[current.format]" class="w-100 border border-bottom-0">
                    </div>
                    <!--Specifications-->
                    <div class="col-7">
                        <ul class="list-group">
                            <li class="list-group-item p-2">{{consts.AUTHOR[consts.AUTHOR.length-1]}}: <span class="h5">{{ current.author }}</span></li>
                            <li class="list-group-item p-2">{{consts.FIELDS.PUBLICATION[consts.FIELDS.PUBLICATION.length-1]}}: <span class="h5">{{ current.publication }}</span></li>
                            <li class="list-group-item p-2">{{consts.FIELDS.DIFFICULT[consts.FIELDS.DIFFICULT.length-1]}}: <span class="h5">{{ consts.FIELDS.DIFFICULT[current.difficult] }}</span></li>
                            <li class="list-group-item p-2" v-if="current.format == 1">
                                <a :href="linkToYouTube" target="_blank" class="h5 underlined">{{consts.PAGES.ARTICLE[1]}}</a>
                            </li>
                            <li class="list-group-item p-2" v-if="current.format == 2">
                                <a :href="current.link" target="_blank" class="h5 underlined">{{consts.PAGES.ARTICLE[1]}}</a>
                            </li>
                        </ul>
                    </div>
                </section>
<#--                Array.from(document.getElementsByClassName("yt-simple-endpoint")).filter(a => a.id === "video-title").map(a => a.href.substring(32, a.href.indexOf("&", 32))).join(";")-->
                <!--Description-->
                <section class="border-top">
                    <p>{{current.description}}</p>
                    <!--Video/Book-->
                    <div v-if="current.format == 1">
                        <v-frame :playlist-links="current.playlist_links" :current-link="current.link"></v-frame>
                    </div>
                </section>
                <!--Other from this author-->
                <v-carousel-container :same="same" carousel-id="carouselId" :parents-aliases="parentsAliases">{{consts.PAGES.ARTICLE[2]}}</v-carousel-container>
            </article>
        </main>
    </div>
</@templates.base>