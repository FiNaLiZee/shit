<#import "../templates.ftl" as templates/>
<#import "../constants.ftl" as consts/>

<@templates.base>
    <@consts.consts></@consts.consts>
    <script type="application/json">{"catalog": ${catalog}}</script>
    <script type="application/json">{"countedCategories": ${counts}}</script>

    <div class="container">
        <v-catalog :catalog="catalog" :counted-categories="countedCategories" :categories="consts.FIELDS.CATEGORY"
                   :format="consts.FIELDS.FORMAT" :text="consts.PAGES.CATALOG.concat(consts.BUTTONS[0])">
        </v-catalog>
    </div>
</@templates.base>