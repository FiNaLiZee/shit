<#import "../templates.ftl" as templates>
<#import "../constants.ftl" as consts>

<@templates.base>
    <@consts.consts></@consts.consts>
    <script type="application/json">{"catalogResults": ${c_results}}</script>
    <script type="application/json">{"articlesResults": ${a_results}}</script>
    <script type="application/json">{"countedCategories": ${counts}}</script>
    <script type="application/json">{"parentsAliases": ${parentsAliases}}</script>
    <script type="application/json">{"keyword": "${keyword}"}</script>

    <div class="container">
        <main class="p-4">
            <h3>{{consts.PAGES.SEARCH[0]}}: "{{keyword}}"</h3>
            <div class="row">
                <div v-if="catalogResults.length" class="px-4 pt-2 w-100">
                    <h4 class="font-weight-light">{{consts.PAGES.SEARCH[1]}}</h4>
                    <div class="row">
                        <div class="col-3 p-3" v-for="item in catalogResults" :key="item.id">
                            <v-catalog-card :alias="item.alias"
                                            :title="item.title"
                                            :counted-categories="countedCategories[item.id] || []"
                                            :format="consts.FIELDS.FORMAT"
                                            :btn-text="consts.BUTTONS[0]">
                            </v-catalog-card>
                        </div>
                    </div>
                </div>
                <div v-if="articlesResults.length" class="px-4 pt-2 w-100">
                    <h5 class="font-weight-light">{{consts.PAGES.SEARCH[2]}}</h5>
                    <div class="row">
                        <div class="col-3 p-3" v-for="item in articlesResults" :key="item.id">
                            <v-articles-card :alias="item.alias" :title="item.title" :author="item.author"
                                             :format="item.format"
                                             :parents-alias="parentsAliases[item.parent]">
                            </v-articles-card>
                        </div>
                    </div>
                </div>
                <h4 v-if="catalogResults.length == 0 && articlesResults.length == 0" class="font-weight-light p-4">{{consts.PAGES.SEARCH[3]}}</h4>
            </div>
        </main>
    </div>
</@templates.base>
