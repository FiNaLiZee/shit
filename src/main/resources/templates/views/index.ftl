<#import "../templates.ftl" as templates/>
<#import "../constants.ftl" as consts/>

<@templates.base>
    <@consts.consts></@consts.consts>
    <script type="application/json">{"catalog": ${catalog}}</script>
    <script type="application/json">{"categories": ${categories}}</script>

    <div class="container-fluid text-center">
        <div class="row justify-content-center">
            <!--Greets-->
            <section class="col-12">
                <h2 class="my-2">{{consts.PAGES.INDEX[0]}}</h2>
                <h5 class="mb-3 font-weight-light">{{consts.PAGES.INDEX[1]}}</h5>
                <div class="row justify-content-center">
                    <div class="col-3">
                        <a class="btn btn-dark btn-lg btn-block" href="/start">{{consts.PAGES.INDEX[2]}}</a>
                    </div>
                </div>
            </section>
            <!--Main art-->
            <figure class="col-12 my-5"><img src="/assets/img/index-art.png" width="980" alt="main art"></figure>
            <!--Block start-->
            <section class="container-fluid bg-white">
                <div class="container">
                    <h2 class="text-center font-weight-light mb-4">{{consts.PAGES.INDEX[3]}}</h2>
                    <div class="row justify-content-center">
                        <div class="col-9">{{consts.PAGES.INDEX[4]}}</div>
                        <div class="col-6 d-flex justify-content-between py-4">
                            <img src="/assets/img/books.png" width="160" title="Книги" alt="books">
                            <img src="/assets/img/article.png" width="100" title="Статьи" alt="articles">
                            <img src="/assets/img/video.png" class="mt-2" width="150" title="Видео" alt="videos">
                        </div>
                    </div>
                </div>
            </section>
            <!--Block with cards-->
            <section class="container text-center">
                <h2 class="font-weight-light">{{consts.PAGES.INDEX[5]}}</h2>
                <h5 class="font-weight-light mb-4">{{consts.PAGES.INDEX[6]}}</h5>
                <!--Cards-->
                <v-index-card v-for="card in catalog" :key="card.id"
                              :category="consts.FIELDS.CATEGORY[card.category]"
                              :alias="card.alias"
                              :title="card.title"
                              :description="card.description">
                    <template slot="articles">{{consts.FIELDS.FORMAT[0]}}: <b>{{ categories[card.id] ? (categories[card.id]["0"] || 0) : 0 }}</b></template>
                    <template slot="videos">{{consts.FIELDS.FORMAT[1]}}: <b>{{ categories[card.id] ? (categories[card.id]["1"] || 0) : 0 }}</b></template>
                    <template slot="books">{{consts.FIELDS.FORMAT[2]}}: <b>{{ categories[card.id] ? (categories[card.id]["2"] || 0) : 0 }}</b></template>
                    <template slot="demand">{{consts.FIELDS.DEMAND[card.demand]}} <span class="text-lowercase">{{consts.FIELDS.DEMAND[consts.FIELDS.DEMAND.length-1]}}</span></template>
                    {{consts.BUTTONS[0]}}
                </v-index-card>
            </section>
            <!--Block start-->
            <section class="container-fluid bg-white">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-6 text-center">
                            <h2 class="font-weight-light mb-2">{{consts.PAGES.INDEX[7]}}</h2>
                            <p v-html="consts.PAGES.INDEX[8]"></p>
                            <a href="/catalog" class="btn btn-outline-dark mt-4">{{consts.PAGES.INDEX[9]}}</a>
                        </div>
                        <div class="col-1 justify-content-start">
                            <img src="/assets/img/why.png" width="115" height="230">
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</@templates.base>