<#import "../templates.ftl" as templates/>
<#import "../constants.ftl" as consts/>

<@templates.base>
    <@consts.consts></@consts.consts>
    <script type="application/json">{"current": ${current}}</script>
    <script type="application/json">{"categories": ${categories}}</script>
    <script type="application/json">{"childs": ${childs}}</script>
    <script type="application/json">{"articles": ${articles}}</script>

    <div class="container">
        <main class="row">
            <!--Tabs bar-->
            <aside class="nav flex-column col-2 p-0 border-right">
                <a class="nav-link active" data-toggle="pill" href="#main">{{consts.PAGES.CURRENT[0]}}</a>
                <a class="nav-link" data-toggle="pill" href="#basics">{{consts.PAGES.CURRENT[1]}}</a>
                <a v-for="i in uniqueCategories.length" :href="'#'+consts.FIELDS.CATEGORY[uniqueCategories[i-1]]"
                   class="nav-link" data-toggle="pill">{{consts.FIELDS.CATEGORY[uniqueCategories[i-1]]}}</a>
            </aside>
            <article class="tab-content col-10 p-0">
                <!--Main-->
                <div class="tab-pane fade show active" id="main">
                    <div class="p-3">
                        <img width="50%" class="float-left mr-3" :src="'/assets/img/catalog/'+current.alias+'/main.png'" alt="poster">
                        <h3>{{consts.PAGES.CURRENT[3]}}</h3>
                        <div class="tab-content d-flex border rounded mb-5" style="min-height: 211px;">
                            <ul class="tab-pane fade show active list-group w-100" id="first" style="border-radius: .25rem 0 0 .25rem">
                                <li class="list-group-item">{{consts.FIELDS.CATEGORY[current.category]}}:
                                    <span class="h5">{{current.title}}</span>
                                </li>
                                <li class="list-group-item">{{consts.FIELDS.DEMAND[consts.FIELDS.DEMAND.length-1]}}:
                                    <span class="h5">{{consts.FIELDS.DEMAND[current.demand]}}</span>
                                </li>
                                <li class="list-group-item">{{consts.FIELDS.SUITABLE[consts.FIELDS.SUITABLE.length-1]}}:
                                    <span class="h5">{{consts.FIELDS.SUITABLE[current.suitable]}}</span>
                                </li>
                                <li class="list-group-item">{{consts.FIELDS.TIME[consts.FIELDS.TIME.length-1]}}<v-tooltip>{{consts.FIELDS.TIME[2]}}</v-tooltip> :
                                    <span class="h5">{{current.time}}</span>
                                </li>
                            </ul>
                            <ul v-if="current.category == 0" class="tab-pane fade list-group w-100" id="second" style="border-radius: .25rem 0 0 .25rem">
                                <li class="list-group-item">{{consts.FIELDS.OOP[consts.FIELDS.OOP.length-1]}}:
                                    <span class="h5">{{consts.FIELDS.OOP[current.oop]}}</span>
                                </li>
                                <li class="list-group-item">{{consts.FIELDS.ABSTRACTION[consts.FIELDS.ABSTRACTION.length-1]}}:
                                    <span class="h5">{{consts.FIELDS.ABSTRACTION[current.abstraction]}}</span>
                                </li>
                                <li class="list-group-item">{{consts.FIELDS.VARS[consts.FIELDS.VARS.length-1]}}:
                                    <span class="h5">{{consts.FIELDS.VARS[current.vars]}}</span>
                                </li>
                                <li class="list-group-item">{{consts.FIELDS.SYNTAX[consts.FIELDS.SYNTAX.length-1]}}:
                                    <span class="h5">{{consts.FIELDS.SYNTAX[current.syntax]}}</span>
                                </li>
                            </ul>
                            <div v-if="current.category == 0" class="nav border flex-column align-content-center" style="border-radius: 0 .25rem .25rem 0">
                                <a data-toggle="tab" href="#first" class="mb-auto active ico-lighter fas fa-chevron-up"></a>
                                <a data-toggle="tab" href="#second" class="ico-lighter fas fa-chevron-down"></a>
                            </div>
                        </div>
                        <div class="my-5" v-html="current.description"></div>
                    </div>
                </div>
                <!--Basics-->
                <div class="tab-pane fade" id="basics">
                    <!--Tabs for basics-->
                    <ul class="nav nav-tabs">
                        <li v-for="i in consts.FIELDS.FORMAT.length-1" class="nav-item">
                            <a :class="[i-1 ? '' : 'active border-left-0', 'nav-link']" :style="i-1 ? '' : 'border-top-left-radius: 0'" data-toggle="tab" :href="'#tab'+i">{{consts.FIELDS.FORMAT[i-1]}}</a>
                        </li>
                        <li class="ml-auto align-self-center mr-3">
                            <input type="search" :placeholder="consts.PLACEHOLDERS[0]" v-model="search"
                                   class="inline-block form-control border-dark" style="height: 28px; width: 130px;">
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div v-for="i in consts.FIELDS.FORMAT.length-1" :class="[ i-1 ? '' : 'show active', 'tab-pane', 'fade']" :id="'tab' + i">
                            <h4 v-if="noResults(i-1)" class="text-center font-weight-light mt-3">{{consts.PAGES.CURRENT[4]}}</h4>
                            <v-current-basic-card v-for="article in searchFilter" :key="article.id" v-if="article.format == i-1"
                                                  :parent="current.alias"
                                                  :format="article.format"
                                                  :alias="article.alias"
                                                  :title="article.title"
                                                  :description="article.description">
                                <template slot="authorText">{{consts.AUTHOR[0]}}: <span class="h5">{{article.author}}</span></template>
                                <template slot="publicationText">{{consts.FIELDS.PUBLICATION[consts.FIELDS.PUBLICATION.length-1]}}: <span class="h5">{{article.publication}}</span></template>
                                <template slot="btnText">{{consts.BUTTONS[0]}}</template>
                            </v-current-basic-card>
                        </div>
                    </div>
                </div>
                <!--Childs-->
                <div v-for="i in consts.FIELDS.CATEGORY.length-2" class="tab-pane fade" :id="consts.FIELDS.CATEGORY[i]">
                    <div class="col-12 px-0">
                        <v-current-category-card v-for="child in childs" v-if="child.category==i" :key="child.id"
                                                 :alias="child.alias"
                                                 :title="child.title"
                                                 :description="child.description">
                            <template slot="videos">{{consts.FIELDS.FORMAT[1]}}: <b>{{ categories[child.id] ? (categories[child.id]["1"] || 0) : 0 }}</b></template>
                            <template slot="books" >{{consts.FIELDS.FORMAT[2]}}: <b>{{ categories[child.id] ? (categories[child.id]["2"] || 0) : 0 }}</b></template>
                            <template slot="suitable" v-if="child.suitable">{{consts.FIELDS.SUITABLE[consts.FIELDS.SUITABLE.length-1]}}</template>
                            <template slot="suitable" v-else>{{consts.PAGES.CURRENT[5]}}</template>
                            <template slot="demand">{{consts.FIELDS.DEMAND[child.demand]}} <span class="text-lowercase">{{consts.FIELDS.DEMAND[consts.FIELDS.DEMAND.length-1]}}</span></template>
                            {{consts.BUTTONS[0]}}
                        </v-current-category-card>
                    </div>
                </div>
            </article>
        </main>
    </div>
</@templates.base>