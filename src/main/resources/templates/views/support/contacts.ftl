<#import "../../templates.ftl" as templates>
<#import "../../constants.ftl" as consts>

<@templates.base>
    <@consts.consts></@consts.consts>

    <div class="container col-4">
        <main class="d-flex justify-content-center">
            <div class="p-4 col-9">
                <h4 class="text-center mb-4">{{consts.PAGES.CONTACTS[0]}}</h4>
                <form action="/support/contacts" method="POST">
                    <div class="form-group">
                        <label class="mb-0">{{consts.PAGES.CONTACTS[1]}}</label>
                        <input name="name" type="text" class="form-control" :placeholder="consts.PAGES.CONTACTS[2]">
                    </div>
                    <div class="form-group">
                        <label class="mb-0">{{consts.PAGES.CONTACTS[3]}}</label>
                        <input name="mail" type="email" class="form-control" :placeholder="consts.PAGES.CONTACTS[4]">
                    </div>
                    <div class="form-group">
                        <label>
                            {{consts.PAGES.CONTACTS[5]}}
                            <textarea name="massage" class="form-control" rows="3" cols="100"></textarea>
                        </label>
                    </div>
                    <button type="submit" class="btn btn-dark">{{consts.BUTTONS[1]}}</button>
                </form>
            </div>
        </main>
    </div>
</@templates.base>