<#import "../../templates.ftl" as templates>
<#import "../../constants.ftl" as consts>

<@templates.base>
    <@consts.consts></@consts.consts>
    <div class="container">
        <main class="p-4">
            <h4 class="mb-4">{{consts.PAGES.FAQ[0]}}</h4>
            <div v-for="i in consts.PAGES.FAQ.length-1" class="col-10 mt-4">
                <v-dropdown-block v-for="question in consts.PAGES.FAQ[i]"
                                  :head="question[0]"><span v-html="question[1]"></span>
                </v-dropdown-block>
            </div>
        </main>
    </div>
</@templates.base>