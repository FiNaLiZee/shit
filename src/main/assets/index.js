import './scss/main.scss'
import Controllers from "./controllers/Controllers";

window.addEventListener("load", () => {
    Vue.prototype.bus = global.bus = new Vue();
    const components = require.context('./', true, /\w+\.(vue)$/);
    components.keys().forEach(filename => {
        let config = components(filename);
        let name = filename.split('/').pop().replace(/\.\w+$/, '').replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
        Vue.component('v-' + name, config.default || config);
        console.log(filename, 'mapped by', 'v-' + name);
    });

    function getData() {
        let data = {};
        let scripts = Array.from(document.querySelectorAll("script, template"))
            .map(node => node.tagName === "TEMPLATE"
                ? Array.from(node.content.querySelectorAll("script")) : node);
        scripts = [].concat.apply([], scripts).filter(s => s.type === "application/json");
        scripts.forEach(script => {
            try {
                data = Object.assign(data, JSON.parse(script.innerHTML));
                script.parentNode.removeChild(script);
            } catch (err) {
                console.error("Wrong json object in script data tag", script);
            }
        });
        console.log(data, "\n_________________________________________________________________________________________");
        return data;
    }

    Controllers.register(require.context('./controllers', true, /\.js$/));
    let data = getData();
    let obj = Controllers.execute(window.location.pathname, data) || {};
    if (Vue) new Vue({el: "#app", ...{...obj, ...{data: {...data, ...obj.data}}}});
});