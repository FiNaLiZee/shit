export default {
    mapping: "^/articles/",
    method: () => ({
        data: {
            prev: null, next: null,
            tree: null,
            type: ["jpg", "jpg", "webp"]
        },
        mounted() {
            let sorted = this.articles.sort((a, b) => a.parent - b.parent);
            let index = sorted.map(e => e.alias === this.current.alias).indexOf(true);
            this.next = index < sorted.length - 1 ? sorted[index + 1].alias : "";
            this.prev = index >= 1 ? sorted[index - 1].alias : "";

            let tree = {};
            this.catalog.forEach(el => tree[el.id] = el);
            this.articles.forEach(a => {
                if (!tree[a.parent].articles) tree[a.parent].articles = [];
                tree[a.parent].articles.push(a);
            });
            Object.keys(tree).map(key => tree[key])
                .filter(obj => obj.parent && tree[obj.parent])
                .forEach(obj => {
                    if (!tree[obj.parent].childs) tree[obj.parent].childs = [];
                    tree[obj.parent].childs.push(obj);
                });
            this.tree = Object.values(tree).filter(e => !e.parent);
        },
        computed: {
            cut() {
                return this.current.title.substr(0,46)
            },
            linkToYouTube() {
                return 'https://www.youtube.com/' + (this.current.playlist ? 'playlist?list=' : 'watch?v=') + this.current.link
            }
        }
    })
}