export default {
    mapping: "^/articles",
    method: () => ({
        data: {
            filters: {CATEGORY: [], FORMAT: [], DIFFICULT: [], PUBLICATION: ["", ""], DEMAND: [], SUITABLE: [], TIME: ["", ""], ABSTRACTION: [], VARS: [], OOP: [], SYNTAX: []},
        },
        mounted() {

        },
        computed: {
            filter() {
                let filteredCatalog = this.catalog;
                let filteredArticles = [];
                let result = [];

                function between(x, min, max) {
                    if (min === "" && max === "") return true
                    if (min !== "" && max === "") return x >= min
                    if (min === "" && max !== "") return x <= max
                    if (min !== "" && max !== "") return x >= min && x <= max
                }

                filteredCatalog = this.catalog
                    .filter(c => this.filters.CATEGORY.length === 0 || this.filters.CATEGORY.includes(c.category))
                    .filter(c => this.filters.DEMAND.length === 0 || this.filters.DEMAND.includes(c.demand))
                    .filter(c => this.filters.SUITABLE.length === 0 || this.filters.SUITABLE.includes(c.suitable))
                    .filter(c => between(c.time, this.filters.TIME[0], this.filters.TIME[1]))
                    .filter(c => this.filters.ABSTRACTION.length === 0 || this.filters.ABSTRACTION.includes(c.abstraction))
                    .filter(c => this.filters.VARS.length === 0 || this.filters.VARS.includes(c.vars))
                    .filter(c => this.filters.OOP.length === 0 || this.filters.OOP.includes(c.oop))
                    .filter(c => this.filters.SYNTAX.length === 0 || this.filters.SYNTAX.includes(c.syntax))

                for (let catalogItem of filteredCatalog) {
                    for (let article of this.articles) {
                        if (catalogItem.id === article.parent) filteredArticles.push(article)
                    }
                }

                result = filteredArticles
                    .filter(a => this.filters.FORMAT.length === 0 || this.filters.FORMAT.includes(a.format))
                    .filter(a => this.filters.DIFFICULT.length === 0 || this.filters.DIFFICULT.includes(a.difficult))
                    .filter(a => between(a.publication, this.filters.PUBLICATION[0], this.filters.PUBLICATION[1]))

                return result
            }
        }
    })
}