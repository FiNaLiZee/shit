let controllers = [];

export default {
    register(components) {
        controllers = controllers.concat(
            ...components.keys().map(filename => components(filename))
                .map(component => Object.keys(component).map(key => component[key])
                    .filter(controller => controller && controller.mapping && controller.method)));
    },
    execute(url, data) {
        let controller = controllers.find(c => Array.isArray(c.mapping)
            ? c.mapping.find(mapping => new RegExp(mapping, "i").test(url))
            : new RegExp(c.mapping, "i").test(url));
        return controller ? controller.method(data) : null;
    }
}