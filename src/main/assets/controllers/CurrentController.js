export default {
    mapping: "^/catalog/",
    method: () => ({
        data: {search: "", uniqueCategories: []},
        methods: {
            noResults(i) {
                return !this.searchFilter.filter(a => a.format === i).length
            }
        },
        mounted() {
            for (let i = 0; i < this.childs.length; i++) {
                if (!this.uniqueCategories.includes(this.childs[i].category)) this.uniqueCategories.push(this.childs[i].category)
            }
            console.log(this.childs)
            console.log(this.uniqueCategories)
        },
        computed: {
            searchFilter() {
                return this.articles.filter(a => this.search.length ? (a.title.toLowerCase().match(this.search.toLowerCase()) || a.author.toLowerCase().match(this.search.toLowerCase())) : true)
            }
        }
    })
}