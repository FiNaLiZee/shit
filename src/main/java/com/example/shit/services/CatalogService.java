package com.example.shit.services;

import com.example.shit.exceptions.InternalServerException;
import com.example.shit.models.CatalogItem;
import com.example.shit.repositories.CatalogRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CatalogService {
    private final CatalogRepository catalogRepository;

    public List<CatalogItem> findAll() {
        return catalogRepository.findAll();
    }

    public Optional<CatalogItem> findByAlias(String alias) {
        return catalogRepository.findByAlias(alias);
    }

    public List<CatalogItem> findAllByParent(Long parent) {
        return catalogRepository.findAllByParent(parent);
    }

    public String getParentAliasById(Long id) {
        return catalogRepository.findById(id).orElseThrow(() -> new InternalServerException("Can't find CatalogItem by ID=" + id)).getAlias();
    }

    public HashMap<Long, String> getCatalogIdAndAliases() {
        HashMap<Long, String> parentsAliases = new HashMap<>();
        List<CatalogItem> catalog = catalogRepository.findAll();
        for (CatalogItem item : catalog) parentsAliases.put(item.getId(), item.getAlias());
        return parentsAliases;
    }

    public List<CatalogItem> search(String keyword) {
        if (keyword != null) return catalogRepository.search(keyword);
        return catalogRepository.findAll();
    }
}