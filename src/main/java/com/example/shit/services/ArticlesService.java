package com.example.shit.services;

import com.example.shit.models.Article;
import com.example.shit.repositories.ArticlesRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ArticlesService {
    private final ArticlesRepository articlesRepository;

    public List<Article> findAll() {
        return articlesRepository.findAll();
    }

    public List<Article> findAllByParent(Long id) {
        return articlesRepository.findAllByParent(id);
    }

    public Optional<Article> findFirstByAlias(String alias) {
        return articlesRepository.findFirstByAlias(alias);
    }

    public List<Article> findAllByAuthor(String author) {
        return articlesRepository.findAllByAuthor(author);
    }

    public Map<Long, Map<Integer, Integer>> countCategories() {
        Map<Long, Map<Integer, Integer>> result = new HashMap<>();
        Iterable<Article> articles = articlesRepository.findAll();
        for (Article article : articles) {
            Map<Integer, Integer> item = result.getOrDefault(article.getParent(), new HashMap<>());
            if (item.size() == 0) result.put(article.getParent(), item);
            int value = item.getOrDefault(article.getFormat(), 0) + 1;
            item.put(article.getFormat(), value);
        }
        return result;
    }

    public List<Article> search(String keyword) {
        if (keyword != null) return articlesRepository.search(keyword);
        return articlesRepository.findAll();
    }
}