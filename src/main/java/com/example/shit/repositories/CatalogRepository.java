package com.example.shit.repositories;

import com.example.shit.models.CatalogItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface CatalogRepository extends JpaRepository<CatalogItem, Long> {
    Optional<CatalogItem> findByAlias(String alias);

    List<CatalogItem> findAllByParent(Long id);

    @Query("SELECT i FROM CatalogItem i WHERE i.title like %:keyword%")
    List<CatalogItem> search(@Param("keyword")  String keyword);
}