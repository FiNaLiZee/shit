package com.example.shit.repositories;

import com.example.shit.models.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ArticlesRepository extends JpaRepository<Article, Long> {
    Optional<Article> findFirstByAlias(String alias);

    List<Article> findAllByParent(Long id);

    List<Article> findAllByAuthor(String author);

    @Query("SELECT a FROM Article a WHERE a.title like %:keyword% or a.author like %:keyword%")
    List<Article> search(@Param("keyword")  String keyword);
}