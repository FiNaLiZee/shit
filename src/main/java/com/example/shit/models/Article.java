package com.example.shit.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Builder
@Table(name = "articles")
@NoArgsConstructor
@AllArgsConstructor
public class Article {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private Long id;
    private Long parent;
    private String alias, title, author, link, playlist_links;
    private Integer format, difficult, publication;

    @Column(columnDefinition = "TEXT")
    private String description;
}