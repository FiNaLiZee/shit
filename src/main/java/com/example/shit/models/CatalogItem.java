package com.example.shit.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Builder
@Table(name = "catalog")
@NoArgsConstructor
@AllArgsConstructor
public class CatalogItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long parent;
    private String alias, title;
    private Integer time, abstraction, category, demand, suitable, syntax, vars, oop;

    @Column(columnDefinition = "TEXT")
    private String description;
}