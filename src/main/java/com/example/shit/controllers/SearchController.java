package com.example.shit.controllers;

import com.example.shit.services.ArticlesService;
import com.example.shit.services.CatalogService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static com.example.shit.utils.StringUtils.json;

@Controller
@RequiredArgsConstructor
public class SearchController {
    private final ArticlesService articlesService;
    private final CatalogService catalogService;

    @PostMapping("/search")
    public String SearchResults(Model model, @RequestParam("keyword") String keyword) {
        model.addAttribute("a_results", json(articlesService.search(keyword)))
                .addAttribute("c_results", json(catalogService.search(keyword)))
                .addAttribute("counts", json(articlesService.countCategories()))
                .addAttribute("keyword", keyword)
                .addAttribute("parentsAliases", json(catalogService.getCatalogIdAndAliases()));

        return "views/search";
    }
}
