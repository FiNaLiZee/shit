package com.example.shit.controllers;

import com.example.shit.services.MailSenderService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequiredArgsConstructor
public class SupportController {
    private final MailSenderService mailSenderService;

    @GetMapping("/support/faq")
    public String faq() {
        return "views/support/faq";
    }

    @GetMapping("/support/about")
    public String about() {
        return "views/support/about";
    }

    @GetMapping("/support/contacts")
    public String contact() {
        return "views/support/contacts";
    }

    @PostMapping("/support/contactss") // plug!!!!!!!!!!
    public String sendMail(Model model, @RequestParam("mail") String mailTo, @RequestParam("name") String name, @RequestParam("massage") String massage) {

        mailSenderService.send(mailTo, "Привет " + name + "!", "Это мы, 4Greens. Спасибо, что написал нам, в скором времени ответим тебе ;)");
        mailSenderService.send("sd103e@mail.ru", "Отзыв на 4greens от " + name + " (" + mailTo + ")", massage);
        model.addAttribute("success", true);

        return "views/support/contacts";
    }
}
