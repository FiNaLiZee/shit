package com.example.shit.controllers;

import com.example.shit.services.ArticlesService;
import com.example.shit.services.CatalogService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import static com.example.shit.utils.StringUtils.json;

@Controller
@RequiredArgsConstructor
public class ArticlesController {
    private final ArticlesService articlesService;
    private final CatalogService catalogService;

    @GetMapping("/articles")
    public String articles(Model model) {
        model.addAttribute("articles", json(articlesService.findAll()))
                .addAttribute("catalog", json(catalogService.findAll()))
                .addAttribute("parentsAliases", json(catalogService.getCatalogIdAndAliases()));

        return "views/articles";
    }
}