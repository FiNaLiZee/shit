package com.example.shit.controllers;

import com.example.shit.services.ArticlesService;
import com.example.shit.services.CatalogService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import static com.example.shit.utils.StringUtils.json;

@Controller
@RequiredArgsConstructor
public class CatalogController {
    private final ArticlesService articlesService;
    private final CatalogService catalogService;

    @GetMapping("/catalog")
    public String catalog(Model model) {
        model.addAttribute("catalog", json(catalogService.findAll()))
                .addAttribute("counts", json(articlesService.countCategories()));
        return "views/catalog";
    }
}