package com.example.shit.controllers;

import com.example.shit.exceptions.NotFoundException;
import com.example.shit.models.Article;
import com.example.shit.services.ArticlesService;
import com.example.shit.services.CatalogService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

import static com.example.shit.utils.StringUtils.json;

@Controller
@RequiredArgsConstructor
public class ArticleController {
    private final CatalogService catalogService;
    private final ArticlesService articlesService;

    @GetMapping("/articles/{alias}")
    public String article(Model model, @PathVariable String alias) {
        Article current = articlesService.findFirstByAlias(alias).orElseThrow(NotFoundException::new);
        List<Article> sameArticles = articlesService.findAllByAuthor(current.getAuthor());
        sameArticles.remove(current);

        model.addAttribute("current", json(current))
                .addAttribute("parentsAliases", json(catalogService.getCatalogIdAndAliases()))
                .addAttribute("catalog", json(catalogService.findAll()))
                .addAttribute("articles", json(articlesService.findAll()))
                .addAttribute("same", json(sameArticles));
        return "views/article";
    }
}