package com.example.shit.controllers;

import com.example.shit.exceptions.NotFoundException;
import com.example.shit.models.CatalogItem;
import com.example.shit.services.ArticlesService;
import com.example.shit.services.CatalogService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import static com.example.shit.utils.StringUtils.json;

@Controller
@RequiredArgsConstructor
public class CurrentController {
    private final CatalogService catalogService;
    private final ArticlesService articlesService;

    @GetMapping("/catalog/{alias}")
    public String current(Model model, @PathVariable String alias) {
        CatalogItem current = catalogService.findByAlias(alias).orElseThrow(NotFoundException::new);
        model.addAttribute("current", json(current))
                .addAttribute("categories", json(articlesService.countCategories()))
                .addAttribute("childs", json(catalogService.findAllByParent(current.getId())))
                .addAttribute("articles", json(articlesService.findAllByParent(current.getId())));
        return "views/current";
    }
}