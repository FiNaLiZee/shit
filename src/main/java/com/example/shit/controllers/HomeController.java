package com.example.shit.controllers;

import com.example.shit.services.ArticlesService;
import com.example.shit.services.CatalogService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import static com.example.shit.utils.StringUtils.json;

@Controller
@RequiredArgsConstructor
public class HomeController {
    private final CatalogService catalogService;
    private final ArticlesService articlesService;

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("catalog", json(catalogService.findAll()))
                .addAttribute("categories", json(articlesService.countCategories()));
        return "views/index";
    }
}